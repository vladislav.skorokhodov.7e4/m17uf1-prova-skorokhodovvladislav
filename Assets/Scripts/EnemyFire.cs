using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFire : MonoBehaviour
{

    public float firerate = 1;
    private float insidefirerate;
    private float nextFire = 0f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    public float bulletForce = 20;

    // Update is called once per frame
    void Update()
    {
        insidefirerate = 1 / firerate;
        if (Time.time > nextFire)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        nextFire = Time.time + insidefirerate;
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(-firePoint.up * bulletForce, ForceMode2D.Impulse);


    }
}
