using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFregate : MonoBehaviour
{

    public float moveSpeed = 5f;
    Vector2 movement = new Vector2(0, -1);
    public Rigidbody2D rb;
    public int fregateLifes = 5;

    // Update is called once per frame
    void Update()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        if (fregateLifes <= 0)
        {
            Destroy(gameObject);
            GameManager.Instance.addScore(20);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Bullet"))
        {
            GameObject pl = collision.gameObject;
            fregateLifes--;
        }

    }
}
