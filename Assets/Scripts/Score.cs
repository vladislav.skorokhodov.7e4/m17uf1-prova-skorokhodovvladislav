using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public Text ScoreCounterText;

    // Update is called once per frame
    void Update()
    {
        ScoreCounterText.text = "Score: " + GameManager.Instance.score;
    }
}
